import React, { useState, useMemo, useEffect } from 'react';
import { Modal } from './index';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import Button from '../Button';

export default {
  title: 'Modal',
  component: Modal,
  decorators: [withKnobs],
};
function KnobsModal() {
  const [state, setState] = useState(false);
  const title = text('title', '标题');
  const child = text('children', 'sdsdsssda');
  const confirm = boolean('confirm', true);
  const okText = text('okText', '');
  const cancelText = text('cancelText', '');
  return (
    <div>
      <Modal
        refCallback={action('refcallback')}
        stopScroll={boolean('stopScroll', true)}
        delay={number('delay', 200)}
        closeButton={boolean('closeButton', true)}
        mask={boolean('mask', true)}
        maskClose={boolean('maskClose', true)}
        callback={action('callback')}
        cancelText={cancelText}
        okText={okText}
        confirm={confirm}
        title={title}
        parentSetState={setState}
        visible={state}
      >
        {child}
      </Modal>
      <Button onClick={() => setState(!state)}>toggle</Button>
    </div>
  );
}
export const knobsModal = () => <KnobsModal></KnobsModal>;

export const noMask = () => {
  const [state, setState] = useState(false);
  return (
    <div>
      <Modal visible={state} parentSetState={setState} mask={false}>
        nomask
      </Modal>
      <Button onClick={() => setState(!state)}>noMask</Button>
    </div>
  );
};

export const anyncClose = () => {
  const [seconds, setSeconds] = useState(5);
  const [state, setState] = useState(false);
  const [loading, setLoading] = useState(false);
  const onLoading = () => {
    setLoading(true);
    setSeconds(5);
    const timer = setInterval(() => {
      setSeconds((n) => {
        if (n === 0) {
          setLoading(false);
          setState(false);
          clearInterval(timer);
        }
        return n - 1;
      });
    }, 1000);
	};
	
  return (
    <>
      <Modal
        visible={state}
        parentSetState={setState}
        onOk={onLoading}
				onCancel={()=>setState(false)}
				confirmLoading={loading}
      >
        {loading ? (
          <>
            <span style={{ color: 'green' }}>{seconds}</span>秒后关闭弹窗
          </>
        ) : (
          '点击确认或者取消'
        )}
      </Modal>
      <Button onClick={() => setState(!state)}>onOkOrOnCancel</Button>
    </>
  );
};
