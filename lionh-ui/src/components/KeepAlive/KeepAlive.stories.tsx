import React from "react";
import App from "./index";
import {
	withKnobs,
	text,
	boolean,
	color,
	select,
} from "@storybook/addon-knobs";

export default {
	title: "KeepAlive",
	component: App,
	decorators: [withKnobs],
};

export const knobsKeepAlive = () => (
    <App />
);